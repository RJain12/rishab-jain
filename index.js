var requirejs = require('requirejs');

requirejs.config({
    nodeRequire: require
});

requirejs([
	'src/server'
], function(
	...args
) {
	(async function(
		server
	) {
		// launch
		await server.init();
	})(...args);
});
