define([
	'express',
	'path',
	'http'
], function(
	express,
	path,
	http
) {
	return {
		app: null,
		server: null,

		redirectors: {
			'/pages/404'     : '/404',
			'/pages/404.html': '/404',
			'/404.html'      : '/404',
            
            '/html/pcdls'   : '/pcdls',
            '/html/pcdls.html'   : '/pcdls',
            '/pcdls.html'    : '/pcdls',
            
			'/pages/500'     : '/500',
			'/pages/500.html': '/500',
			'/500.html'      : '/500',
			
			'/pages/Samyak'     : '/Samyak',
			'/pages/Samyak.html': '/Samyak',
			'/Samyak.html'      : '/Samyak',

			'/index'     : '/',
			'/index.html': '/',
			
			'/html/chembalancer' : '/chembalancer',
			'/html/chembalancer.html' : '/chembalancer',
			'/chembalancer.html' : '/chembalancer',

			// backwards compatibility
			// '/rjain': '/developers/rjain',
		},

		init: async function() {
			console.log('[WEB] Starting web server');

			var app = require('express')();
			this.app = app;
			this.server = http.Server(app);

			app.use(express.static(path.join(process.cwd(), 'src', 'public')));

			// all redirects
			Object.keys(this.redirectors).forEach((source) => {
				var target = this.redirectors[source];

				app.use(source, function(req, res) {
					res.redirect(target);
				});
			})

			// main pages
			app.get('/', function(req, res) {
				res.sendFile(path.join(process.cwd(), 'src', 'public', 'html', 'index.html'));
			});
			// project pages
            app.get('/pcdls', function(req, res) {
				res.sendFile(path.join(process.cwd(), 'src', 'public', 'html', 'pcdls.html'));
			});
			
			app.get('/samyak', function(req, res) {
				res.sendFile(path.join(process.cwd(), 'src', 'public', 'html', 'samyak.html'));
			});
			
			app.get('/chembalancer', function(req, res) {
				res.sendFile(path.join(process.cwd(), 'src', 'public', 'html', 'chembalancer.html'));
			});


			// developer pages
		
			// mount sub-app
			//await feed.init(this.server);
			//app.use('/feed', feed.app);

			// misc routes
			app.get('/404', function(req, res) {
				res.sendFile(path.join(process.cwd(), 'src', 'public', 'html', '404.html'));
			});
			app.get('/500', function(req, res) {
				res.sendFile(path.join(process.cwd(), 'src', 'public', 'html', '500.html'));
			});

			///// error handlers
			// 404 - nothing else to run
			app.use(function (req, res, next) {
				res.status(404);
				res.redirect('/404');
				//sf('public/pages/404.html', res);
			});
			// 500 - internal server error
			app.use(function (err, req, res, next) {
				console.error(err.stack)
				res.status(500);
				res.redirect('/500');
			});

			///// run
			this.server.listen(process.env.PORT || 3000, () => {
				console.log(`[WEB] Live on port ${process.env.PORT || 3000}`);
			});
		}
	}
});
