// bread
$('#btnGoTwitter').click(function() {
	window.open('https://twitter.com/RishabJainK')
});

$('#btnGoInstagram').click(function() {
	window.open('https://www.instagram.com/rishabjaink/');
});

$('#btnGoPCDLS').click(function() {
	window.location.href = '/pcdls.html';
});

$('#btnGoHome').click(function() {
	window.location.href = '/index.html';
});

$('#btnGoEmail').click(function() {
	window.open('mailto:samyaksciencesociety@gmail.com');
});

$('#btnGoSamyak').click(function() {
	window.location.href = '/samyak.html';
});


// all (init stuff)
$('.ui.accordion').accordion();

console.log('Loaded');
